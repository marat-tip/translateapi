package com.proger.pets.translator.configuration

import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RequestHistoryConfiguration {

    @Bean
    fun someFilterRegistration(requestHistoryFilter: RequestHistoryStoringFilter) =
        FilterRegistrationBean<RequestHistoryStoringFilter>().apply {
            filter = requestHistoryFilter
            addUrlPatterns("/translate/*")
            order = 1
        }
}