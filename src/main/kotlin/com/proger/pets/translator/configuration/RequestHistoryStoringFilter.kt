package com.proger.pets.translator.configuration

import com.proger.pets.translator.model.TranslationRequest
import com.proger.pets.translator.model.repository.TranslationRequestRepository
import org.apache.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.util.ContentCachingRequestWrapper
import org.springframework.web.util.ContentCachingResponseWrapper
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class RequestHistoryStoringFilter(private val translationRequestRepository: TranslationRequestRepository) : OncePerRequestFilter() {
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val responseWrapper = ContentCachingResponseWrapper(response)
        val requestWrapper = ContentCachingRequestWrapper(request)
        var translationRequestId = 0L
        try {
            translationRequestId = saveRequestAndGetId(requestWrapper)
        } catch (ex: KotlinNullPointerException) {
            response.status = HttpStatus.SC_BAD_REQUEST
        }

        filterChain.doFilter(requestWrapper, responseWrapper)
        if (translationRequestId != 0L) {
            updateTranslationRequestWithResponse(responseWrapper, translationRequestId)
        }
    }

    private fun updateTranslationRequestWithResponse(response: ContentCachingResponseWrapper, requestId: Long) {
        val translationRequest = translationRequestRepository.findById(requestId).get()
        translationRequest.response = String(response.contentAsByteArray)
        translationRequestRepository.save(translationRequest)
        response.copyBodyToResponse()
    }

    private fun saveRequestAndGetId(requestWrapper: ContentCachingRequestWrapper): Long {
        val parameterMap = requestWrapper.parameterMap
        val fromLang = parameterMap["from"]!![0]!!
        val toLang = parameterMap["to"]!![0]!!
        val text = parameterMap["text"]!![0]!!

        val savedRequest = translationRequestRepository.save(
            TranslationRequest(id = 0, languagePair = "$fromLang-$toLang", text = text, response = "")
        )
        return savedRequest.id
    }
}