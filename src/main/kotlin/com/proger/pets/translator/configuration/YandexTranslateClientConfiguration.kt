package com.proger.pets.translator.configuration

import com.proger.pets.translator.model.YandexTranslateService
import org.jboss.resteasy.client.jaxrs.ResteasyClient
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.ws.rs.core.UriBuilder


@Configuration
class YandexTranslateClientConfiguration {
    @Value("\${yandex.translate.url}")
    private lateinit var yandexTranslateUrl: String

    @Value("\${yandex.apikey}")
    private lateinit var apiKey: String

    @Bean
    fun restEasyClientBean(): YandexTranslateService {
        val client = ResteasyClientBuilder.newClient() as ResteasyClient
        val target = client.target(UriBuilder.fromUri(yandexTranslateUrl))
                .queryParam("key", apiKey)
        val proxy = target.proxy(YandexTranslateService::class.java)
        return proxy
    }
}