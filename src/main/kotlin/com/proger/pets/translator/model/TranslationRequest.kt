package com.proger.pets.translator.model

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "REQUEST_HISTORY")
@SequenceGenerator(sequenceName = "SEQ_REQ_HISTORY", name = "REQ_HISTORY_GEN_SEQ", allocationSize = 1)
data class TranslationRequest(
    @Id
    @Column(name = "REQUEST_ID")
    @GeneratedValue(generator = "REQ_HISTORY_GEN_SEQ", strategy = GenerationType.SEQUENCE)
    val id: Long,

    @Column(name = "REQUEST_TIME")
    val requestTime: LocalDateTime = LocalDateTime.now(),

    @Column(name = "TEXT")
    val text: String,

    @Column(name = "LANGUAGE_PAIR")
    val languagePair: String,

    @Column(name = "RESPONSE")
    var response: String
)