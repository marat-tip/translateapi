package com.proger.pets.translator.model

//"comment" because of https://tech.yandex.com/translate/doc/dg/concepts/design-requirements-docpage/
class TranslationResponse(val translatedText: String,
                          val comment: String = "Powered by Yandex.\nCheck http://translate.yandex.com.")