package com.proger.pets.translator.model

class YandexTranslateResponse(val code: Int, val lang: String, val text: List<String>){
    constructor(): this(0, "", emptyList())
}