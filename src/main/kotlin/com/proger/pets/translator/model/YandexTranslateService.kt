package com.proger.pets.translator.model

import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/")
interface YandexTranslateService {
    @POST
    @Path("/translate")
    @Produces(MediaType.APPLICATION_JSON)
    fun translateText(@QueryParam("text") text: String,
                      @QueryParam("lang") langPair: String,
                      @QueryParam("format") textType: String = "plain"): YandexTranslateResponse
}