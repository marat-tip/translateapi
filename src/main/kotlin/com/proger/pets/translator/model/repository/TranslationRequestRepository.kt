package com.proger.pets.translator.model.repository

import com.proger.pets.translator.model.TranslationRequest
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TranslationRequestRepository : CrudRepository<TranslationRequest, Long>