package com.proger.pets.translator.rest

import com.proger.pets.translator.model.TranslationResponse
import com.proger.pets.translator.service.TranslationService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.regex.Pattern
import java.util.regex.Pattern.UNICODE_CHARACTER_CLASS
import java.util.stream.Collectors

@RestController
@RequestMapping("/translate")
class TranslateController(val translationService: TranslationService) {
    companion object{
        private const val BY_WORD_REGEX = "([\\W]\\b)"
    }

    @GetMapping
    fun translateText(@RequestParam text: String,
                      @RequestParam("from") fromLang: String,
                      @RequestParam("to") toLang: String): TranslationResponse {
        val translatedText = text.split(Pattern.compile(BY_WORD_REGEX, UNICODE_CHARACTER_CLASS))
            .parallelStream()
            .map { translationService.translateText(it, fromLang, toLang) }
            .collect(Collectors.joining(" "))
        return TranslationResponse(translatedText)
    }
}