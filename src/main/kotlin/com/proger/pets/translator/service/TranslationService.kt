package com.proger.pets.translator.service

import com.proger.pets.translator.model.YandexTranslateService
import org.springframework.stereotype.Service

@Service
class TranslationService(private val yandexTranslate: YandexTranslateService) {

    fun translateText(text: String, from: String, to: String): String {
        return yandexTranslate.translateText(text, "$from-$to").text[0]
    }
}